# Thesis

This is the Git repository for my thesis on "streaming video completions using 
a Tensor-Networked Kalman Filter". In the folder `Matlab_Code`, the matlab files
that were used in this thesis can be found.
To showcase how you can perform the video completion, an `example` file was 
created. If you choose not to use this file, don't forget to run the `setup`file 
first to set up the path variables in MATLAB.

In the `Documents` folder the thesis and the corresponding literature study can
be found.
