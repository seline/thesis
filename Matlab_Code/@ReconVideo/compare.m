function reconVideo = compare(reconVideo,origVideo)
%compare(reconVideo,origVideo) compare the frames of the reconstructed
%   video with the frames of the original video. It does so by updating the
%   relError and PNSR values of the ReconVideo object.
%
%INPUT
%   reconVideo : ReconVideo object.
%   origVideo : VideoData object.
%OUTPUT
%   reconVideo : updated ReconVideo object, with new values for PSNR and
%   relative error.

relError = zeros(1,reconVideo.NumFrames);
PSNR = zeros(1,reconVideo.NumFrames);


for k = 1:reconVideo.NumFrames
    reconFrame = reconVideo.frame(k);
    origFrame = origVideo.frame(origVideo.CorruptTime+k-1);
    relError(1,k) = normTens(double(reconFrame-origFrame))/normTens(double(origFrame));
    
    PSNR(1,k) = psnr(reconFrame,origFrame);
end

reconVideo.pPSNR = PSNR;
reconVideo.pRelError = relError;

end