classdef ReconVideo
    %% Class Properties
    properties (Access = public)
        SizeFrame(1,2) {mustBeInteger,mustBeNonnegative,mustBeFinite}
        Frames(:,1) cell
        OrigVideo
        NumFrames
        Type % uint8, vector, double etc.
        SimulationTime
        Mask
        Color
        P
    end
    properties (Dependent)
        RelError
        PSNR
    end
    properties (Access = private)
        pRelError
        pPSNR
    end
    %% Class Methods
    methods
        %% Constructor
        function [obj,kalmanFilter] = ReconVideo(video,kalmanFilter)
            
            %% Initilization
            obj.NumFrames = video.NumFrames - video.CorruptTime + 1;
            obj.Frames = cell(obj.NumFrames,1);
            obj.SizeFrame = video.SizeFrame;
            obj.OrigVideo = video.Video;
            obj.Mask = kalmanFilter.Mask;
            obj.Color = video.Color;
            obj.P = cell(obj.NumFrames,1);
            %% Performe computations
            t1 = 0;   % set time
            lengthFrame = prod(obj.SizeFrame);
            for k = 1:obj.NumFrames
                t0 = tic;
                kalmanFilter = kalmanFilter.Update(k);  
                t1 = t1 +toc(t0);
                %% Convert to video image
                frame = uint8(kalmanFilter.getFrame(lengthFrame) + ...
                    video.ForeGround * video.BackGround);

                obj.Frames{k} = reshape(frame,video.SizeFrame(1),video.SizeFrame(2),[]);
                %% Save diagonal entries P
                obj.P{k} = diagP(kalmanFilter);
            end
            obj.SimulationTime = t1/obj.NumFrames;
            %% calculate error
            obj = compare(obj,video);   
        end
        
        %% simple methods
        function [frameVector] = getFrameVectors(obj)
            frameVector = cell(obj.numFrames,1);
            for k = 1:obj.numFrames
                frameVector{k,1} = reshape(obj.Frames{k,1},obj.sizeFrame(1) * ...
                    obj.sizeFrame(2),1);
            end
        end
        
         function [obj] = frames2double(obj)
            for k = 1:obj.NumFrames
                obj.Frames{k,1} = double(obj.Frames{k,1});
            end
            obj.Type = 'double';
        end
        
        function [obj] = frames2uint8(obj)
            if isa(obj.Frames{1},'uint8')
                return
            else
                for k = 1:obj.numFrames
                    obj.Frames{k,1} = uint8(obj.Frames{k,1});
                end
            end
            obj.Type = 'uint8';
        end
        
        function [frame] = frameVector(obj,k,varargin)
                % ADAPT for RGB
            if k > obj.NumFrames
                warning('Index out of bounds');
                return
            end
            if obj.Color
                rgb_index = varargin{1};
                frame = double(reshape(obj.Frames{k}(:,:,rgb_index),...
                    prod(obj.SizeFrame),1));
            else
                frame = double(reshape(obj.Frames{k},prod(obj.SizeFrame),1));
            end
        end
        
        function [frame] = frame(obj,k,varargin)
           frame = obj.Frames{k}; 
        end
        %% Save to video file
        function [v] = saveVideo(obj,filename)
            v = VideoWriter(filename);
           % v.Quality = 90;
            v.FrameRate = obj.OrigVideo.FrameRate;
            obj = obj.frames2uint8;
            open(v);
            for k = 1:obj.NumFrames
                writeVideo(v,obj.Frames{k,1});
            end
            close(v);
        end
        
        function saveMasked(obj,filename)
            v = VideoWriter(filename, 'Grayscale AVI');
         %   v.Quality = 90;
            v.FrameRate = obj.OrigVideo.FrameRate;
            obj = obj.frames2double;
            mask = reshape(full(obj.Mask),obj.SizeFrame(1),obj.SizeFrame(2));
            open(v);
            for k = 1:obj.NumFrames
                frame = uint8(mask.*obj.Frames{k,1});
                writeVideo(v,frame);
            end
            close(v);
        end
        
        %% Play video in matlab
        function play(obj)
            if obj.Color
                frames = zeros(obj.SizeFrame(1),obj.SizeFrame(2),3,obj.NumFrames-1,'uint8');
                for k = 1:obj.NumFrames-1
                    frames(:,:,:,k) = obj.Frames{k};
                end
            else
                frames = zeros(obj.SizeFrame(1),obj.SizeFrame(2),obj.NumFrames-1,'uint8');
                for k = 1:obj.NumFrames-1
                    frames(:,:,k) = obj.Frames{k};
                end
            end
            
            implay(frames,obj.OrigVideo.FrameRate)
        end
        %% Get methodsS
        function value = get.RelError(obj)
            value = obj.pRelError;
        end
        function value = get.PSNR(obj)
            value = obj.pPSNR;
        end
        
        %% Methods definition
        reconVideo = compare(reconVideo,origVideo)
        
        
    end
    
end