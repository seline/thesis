
close all
clearvars;
%% Set Variables
ranks = [30];
numRanks = length(ranks);
videoName = "TownCentre";
BW = 15;
missPix = 75;
%% Load Data

str = sprintf("./Data/simulations/%s/gray_rankX_%d_BW_%d_mask_%d_simTime_100.mat",...
    videoName,ranks,BW,missPix);
videoData = load(str);
video = videoData.video;
reconVideo = videoData.reconVideo;
filt = videoData.filt;

frameSize = reconVideo.SizeFrame;
mask = full(reshape(filt.Mask,frameSize));
numFrames = reconVideo.NumFrames;
T0 = video.CorruptTime;
v = video.Video;
backGround = reshape(video.BackGround,frameSize)/255;


%% Generate initial data
frames = cell(numFrames+1,1);
frames{1} = read(v,T0-1);
maskedFrames = cell(numFrames,1);
J = cell(numFrames,1);
for k = 1:numFrames
    frames{k+1} = read(v,(k-1)+T0);
    maskedFrames{k} = mask.*(im2double(frames{k+1})-backGround);
    J{k} = mask;
end

figure
imshow(frames{51},'Border','Tight')

figure
imshow(reconVideo.Frames{50},'Border','Tight')

figure
imshow(im2uint8(maskedFrames{50}),'Border','Tight');


%% Simulate PLMS

lambda = 0.8;
mu =1;
X0 = im2double(frames{1})-backGround;
tic;
recoveredFrames = PLMS(maskedFrames,J,X0,lambda,mu);
T_plms = toc/numFrames
%% Calculate error
relErrorPLMS = zeros(1,numFrames);
PSNR_PLMS = zeros(1,numFrames);
for k = 1:numFrames
    recoveredFrames{k} = im2uint8(recoveredFrames{k}+backGround);
    relErrorPLMS(k) = norm(double(recoveredFrames{k})-double(frames{k+1}),'fro')/...
        norm(double(frames{k+1}),'fro');
    PSNR_PLMS(k) = psnr(frames{k+1},recoveredFrames{k});
end

%% Check results
figure
imshow(im2uint8(recoveredFrames{50}),'Border','Tight')


%% Comparison figure
numFrames = videoData.reconVideo.NumFrames;
x = 0:numFrames-1;
figure
plot(x,reconVideo.RelError,'.-', 'MarkerSize', 12) 
hold on
plot(x,relErrorPLMS,'.-', 'MarkerSize', 12)
legend('Kalman','PLMS')
xlabel('Frame')
ylabel('Relative Error');
grid on
set(gca,'FontSize',14)

figure
plot(x,reconVideo.PSNR,'.-', 'MarkerSize', 12)
hold on
plot(x,PSNR_PLMS,'.-', 'MarkerSize', 12)
legend('Kalman','PLMS')
xlabel('Frame')
ylabel('PSNR');
grid on
set(gca,'FontSize',14)
%%
% 
% video = VideoReader('videos/TownCentreXVID_480p_Gray.avi');
% v = VideoData(video,200);
% %% Create mask
% 
% [measIndex,mask] = createMask([480 720],0.5,'m');
% mask = full(mask);
% %% Save measured frames
% backGround = reshape(v.BackGround,480,720);
% N = 100;
% T0 = 100;
% frames = cell(N+1,1);
% frames{1} = read(video,T0);
% maskedFrames = cell(N,1);
% J = cell(N,1);
% for k = 1:N
%     frames{k+1} = read(video,k+T0);
%     maskedFrames{k} = mask.*(im2double(frames{k})-backGround);
%     J{k} = mask;
% end
% 
% %% PLMS
% lambda = 0.8;
% mu =1;
% X0 = im2double(frames{1})-backGround;
% recoveredFrames = PLMS(maskedFrames,J,X0,lambda,mu);
% 
% for k = 1:N
%     recoveredFrames{k} = recoveredFrames{k}+backGround;
% end
% %%
% 
% figure
% imshow(im2uint8(recoveredFrames{100}))
% 
% figure
% imshow(frames{101})