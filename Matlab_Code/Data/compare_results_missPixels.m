clearvars;
close all;

%%
ranks = 20;

videoData = cell(2,1);
missPix = [ 75 95];
BW = 20;
videoName = "4p-c0";

%% Load Data
for k = 1:2
    str = sprintf("./Data/simulations/%s/gray_rankX_%d_BW_%d_mask_%d_simTime_100.mat",...
        videoName,ranks,BW,missPix(k));
    videoData{k} = load(str);
    videoData{k}.reconVideo.SimulationTime
end

%% Compare results
close all
numFrames = videoData{1}.reconVideo.NumFrames;
x = 0:numFrames-1;
figure(1)
for k = 1:2
   plot(x,videoData{k}.reconVideo.RelError,'.-','MarkerSize',12) 
    hold on
end
grid on
%axis([0 numFrames-1,0.05 0.12])
legend('\beta = 75%','\beta = 95%')
ylabel('Relative Error')
xlabel('Frame')
set(gca,'FontSize',14)
%% PSNR

figure(2)
for k = 1:2
   plot(x,videoData{k}.reconVideo.PSNR,'.-','MarkerSize',12) 
    hold on
end
grid on
%axis([0 numFrames-1,25 31])
legend('\beta = 75%','\beta = 95%')
ylabel('PSNR')
xlabel('Frame')
set(gca,'FontSize',14)