clearvars;
close all;

%%
ranks = [20, 30];

videoData = cell(4,1);
missPix = [95];
BW = [20,15];
videoName = ["4p-c0","TownCentre"];

%% Load Data
for k = 1:2
    str = sprintf("./Data/simulations/%s/gray_rankX_%d_BW_%d_mask_%d_simTime_100_color.mat",...
        videoName(k),ranks(k),BW(k),missPix);
    videoData{k} = load(str);
    videoData{k}.reconVideo.SimulationTime
end

for k = 1:2
    str = sprintf("./Data/simulations/%s/gray_rankX_%d_BW_%d_mask_%d_simTime_100.mat",...
        videoName(k),ranks(k),BW(k),missPix);
    videoData{k+2} = load(str);
    videoData{k+2}.reconVideo.SimulationTime
end

%% Compare results
close all
numFrames = videoData{1}.reconVideo.NumFrames;
x = 0:numFrames-1;
figure(1)
p1 = plot(x,videoData{3}.reconVideo.RelError,'o-','MarkerSize',5);
hold on
p2 = plot(x,videoData{4}.reconVideo.RelError,'o-','MarkerSize',5);
p1.Color = [0, 0.4470, 0.7410, 0.4];
p2.Color = [0.8500, 0.3250, 0.0980, 0.4];
for k = 1:2
   p{k} = plot(x,videoData{k}.reconVideo.RelError,'.-','MarkerSize',20) ;
    hold on
end
p{1}.Color = [0, 0.4470, 0.7410, 1];
p{2}.Color = [0.8500, 0.3250, 0.0980, 1];
grid on
axis([0 50,0.03 0.15])
lh = legend([p{1} p{2} p1 p2], {'4p-c0', 'TownCentre','gray','gray'},...
    'Location','NorthWest', 'NumColumns',2);
ylabel('Relative Error')
xlabel('Frame')
set(gca,'FontSize',14)
%% PSNR
close all
figure(2)
p1 = plot(x,videoData{3}.reconVideo.PSNR,'o-','MarkerSize',5);
hold on
p2 = plot(x,videoData{4}.reconVideo.PSNR,'o-','MarkerSize',5);
p1.Color = [0, 0.4470, 0.7410, 0.4];
p2.Color = [0.8500, 0.3250, 0.0980, 0.4];
for k = 1:2
   p{k} = plot(x,videoData{k}.reconVideo.PSNR,'.-','MarkerSize',17); 
    hold on
end
p{1}.Color = [0, 0.4470, 0.7410, 1];
p{2}.Color = [0.8500, 0.3250, 0.0980, 1];
grid on
axis([0 50,24 32])
lh = legend([p{1} p{2} p1 p2], {'4p-c0', 'TownCentre','gray','gray'},...
    'Location','NorthWest', 'NumColumns',2);
ylabel('PSNR')
xlabel('Frame')
set(gca,'FontSize',14)