clearvars;
close all;

%%
ranks = [20 30 40 50];
numRanks = length(ranks);
videoData = cell(numRanks,1);
missPix = 75;
BW = 10;
videoName = "grandcentral";

%% Load Data
for k = 1:numRanks
    str = sprintf("./Data/simulations/%s/gray_rankX_%d_BW_%d_mask_%d.mat",...
        videoName,ranks(k),BW,missPix);
    videoData{k} = load(str);
    videoData{k}.reconVideo.SimulationTime
end

%% Compare results
close all
numFrames = videoData{1}.reconVideo.NumFrames;
x = 0:numFrames-1;
legendInfo = cell(numRanks,1);
figure(1)
for k = 1:numRanks
   plot(x,videoData{k}.reconVideo.RelError,'.-','MarkerSize',12) 
    hold on
    legendInfo{k} = sprintf("R = %d",ranks(k));
end
grid on
axis([0 numFrames-1,0.05 0.12])
legend(legendInfo,'Location','southeast')
ylabel('Relative Error')
xlabel('Frame')
set(gca,'FontSize',14)
%% PSNR

figure(2)
for k = 1:numRanks
   plot(x,videoData{k}.reconVideo.PSNR,'.-','MarkerSize',12) 
    hold on
    legendInfo{k} = sprintf("R = %d",ranks(k));
end
grid on
axis([0 numFrames-1,25 31])
legend(legendInfo,'Location','northeast')
ylabel('PSNR')
xlabel('Frame')
set(gca,'FontSize',14)