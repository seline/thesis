%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN THIS FILE TO SET UP SEARCH PATH %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath('./old'),'-begin');
addpath(genpath('./Data'),'-begin');
addpath(genpath('./Covariance'),'-begin');
addpath(genpath('./Reference_methods'),'-begin');
addpath(genpath('./TT_Functions'),'-begin');
addpath(genpath('./Tensor_Functions'),'-begin');
addpath(genpath('./Video_Functions'),'-begin');
addpath(genpath('./Kalman_Filter'),'-begin');
addpath(genpath('./test'),'-begin');
addpath('.','-begin');