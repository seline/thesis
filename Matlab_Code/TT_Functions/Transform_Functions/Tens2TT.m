function TT = Tens2TT(tens,par,opt)
%Tens2TT(tens,par,opt) transforms a tensor into a tensor-train. This 
%   function is the same as TT_SVD.

TT = TT_SVD(tens,par,opt);



end