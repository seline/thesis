function TT = Vec2TT(vec,q,par,opt)
%Vec2TT(vec,q,par,opt) converts a vector into a tensor-train.
%
%INPUT
%   vec : vector
%   q : quantization parameter
%   par : ranks or epsilon
%   opt : 'r' or 'e'

tens = Vec2Tens(vec,q);
TT = Tens2TT(tens,par,opt);

end