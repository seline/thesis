function TTm = Mat2TTm(mat,q1,q2,par,opt)
%Mat2TTm(mat,q1,q2,par,opt) 

tens = Mat2Tens(mat,q1,q2);
TTm = Tens2TTm(tens,par,opt);

end

