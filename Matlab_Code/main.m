function [reconVideo,kalmanFilter] = main(video,filter,varargin)
%main(video,kalmanFilter) computes the Kalman filter reconstruction of the
%   given corrupted video. 
%
%INPUT
%   video : VideoData object.
%   kalmanFilter : KalmanFilter object
%   ... OR options : KalmanFilter options struct
%   (optional) 's' : silent mode
%OUTPUT
%   reconVideo : ReconVideo object.
%   (opt.) kalmanFilter

%% Perform computations
if isa(filter,'struct')
    kalmanFilter = KalmanFilter(video,filter);
elseif isa(filter,'KalmanFilter')
    kalmanFilter = filter;
end              

reconVideo = ReconVideo(video,kalmanFilter);

%% Sound when finished
if nargin == 2
    load('handel','y','Fs');
    sound(y(1:20000),Fs)
end

end

