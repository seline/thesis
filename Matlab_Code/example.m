% This is a test-file to showcase how the tensor-networked Kalman filter
% can be used to perform video completion.

clearvars
close all;
setup; % set up path variable, comment if already done
%% Load variables
v = VideoReader('videos/TownCentreXVID_480p_Gray.avi'); % load video file
load('q_480x720.mat'); % load quantization parameters: qHeight, qWidth, change for different height/width

%% Set parameters
Tcorrupt = 100 ;        % set time that frames are corrupted
Tend = Tcorrupt + 10;   % set end of simulation time, must be > Tcorrupt
missingPixels = 0.99;   % set percentage of missing pixels
rankX = 30;             % set rank of x[k] (frames)
rankP = 1;              % set rank of P[k], best to keep it 1
bandWidth = 15;         % set bandwidth of W (between 10-20)
rankW = 3;              % set rank of W, depends of bandwidth
foreGround = true;      % use background subtraction [true/false]
%% Initialize
video = VideoData(v,Tend,foreGround,Tcorrupt); % create video data, can add backGround

seed = 5; %ensure all simulations have the same seed for creating the mask
rng(seed);
% Set Kalman filter
[options] = setDefaultOptions(video,missingPixels,qHeight,qWidth,rankX,rankP,...
    rankW, bandWidth);
filt = KalmanFilter(video,options);

%% Simulation
reconVideo = main(video,filt); % use 's' for silent mode

%% Create plots relative error and PSNR
numFrames = reconVideo.NumFrames;
x = 0:numFrames-1;

figure(1)
plot(x,reconVideo.RelError,'.-','MarkerSize',12)
grid on
ylabel('Relative Error')
xlabel('Frame')
set(gca,'FontSize',14)

figure(2)
plot(x,reconVideo.PSNR,'.-','MarkerSize',12)
grid on
ylabel('PSNR')
xlabel('Frame')
set(gca,'FontSize',14)

%% Play video in matlab
reconVideo.play