classdef VideoData
    %% Class Properties
    properties (Access = public)
        SizeFrame(1,2) {mustBeInteger,mustBeNonnegative,mustBeFinite}
        Video
        NumFrames   % can be smaller than number of frames of video
        BackGround = 0;
        CorruptTime = 0;
        ForeGround = true;
        Color = false;
    end
    properties (Dependent)
        VarP
        VarW
    end
    properties (Access = private)
        pVarP
        pVarW
    end
    %% Class Methods
    methods
        %% Constructor
        function [obj] = VideoData(varargin)
            if nargin == 1
                obj.Video = varargin{1};
                obj.SizeFrame = [obj.Video.Height obj.Video.Width];
                obj.NumFrames = obj.Video.NumFrames;
                
                prompt = 'Subtract background [true/false]: ';
                obj.ForeGround = input(prompt);
                
                if strcmp(obj.Video.VideoFormat,'Grayscale')
                    obj.Color = false;
                else
                    obj.Color = true;
                end
                obj.BackGround = getBackground(obj);
                prompt = 'Corruption time: ';
                obj.CorruptTime = input(prompt);
                
            elseif nargin == 2
                obj.Video = varargin{1};
                obj.NumFrames = varargin{2};
                obj.SizeFrame = [obj.Video.Height obj.Video.Width];
                
                prompt = 'Subtract background [true/false]: ';
                obj.ForeGround = input(prompt);
                
                if strcmp(obj.Video.VideoFormat,'Grayscale')
                    obj.Color = false;
                else
                    obj.Color = true;
                end
                obj.BackGround = getBackground(obj);
                prompt = 'Corruption time: ';
                obj.CorruptTime = input(prompt);
                
            elseif nargin == 4
                obj.Video = varargin{1};
                obj.NumFrames = varargin{2};
                obj.SizeFrame = [obj.Video.Height obj.Video.Width];
                

                obj.ForeGround = varargin{3};
                
                if strcmp(obj.Video.VideoFormat,'Grayscale')
                    obj.Color = false;
                else
                    obj.Color = true;
                end
                obj.BackGround = getBackground(obj);

                obj.CorruptTime = varargin{4};
                
            elseif nargin == 5
                obj.Video = varargin{1};
                obj.NumFrames = varargin{2};
                obj.SizeFrame = [obj.Video.Height obj.Video.Width];
                

                obj.ForeGround = varargin{3};
                
                if strcmp(obj.Video.VideoFormat,'Grayscale')
                    obj.Color = false;
                else
                    obj.Color = true;
                end
                obj.BackGround = varargin{5};

                obj.CorruptTime = varargin{4};
            else
                error('Too many inputs');
            end
        end 

        %% methods
        
        %% Extract Frames
        function [frame] = frame(obj,k,varargin)
            if obj.Color
                frame = read(obj.Video,k);
            else
                frame = read(obj.Video,k);
            end
        end
        function [frameVec] = frameVector(obj,k,varargin)
            if obj.Color
                frame = obj.frame(k);
                rgb_index = varargin{1};
                frame = frame(:,:,rgb_index);
                frameVec = double(reshape(frame,[],1));
            else %gray
                frameVec = double(reshape(obj.frame(k),[],1));
            end
        end
        function [v] = saveVideo(obj,filename)
            v = VideoWriter(filename);
           % v.Quality = 90;
            v.FrameRate = obj.Video.FrameRate;
            open(v);
            for k = obj.CorruptTime:obj.NumFrames
                frame = read(obj.Video,k);
                writeVideo(v,frame);
            end
            close(v);
        end
        
       
        %% Update variance values when corruption time is adjusted
        function [obj] = set.CorruptTime(obj,value)
            if obj.CorruptTime ~= value
                obj.CorruptTime = value;
                obj = UpdateVar(obj);
            else
                return;
            end
        end
        
        function value = get.VarP(obj)
            value = obj.pVarP;
        end
        function value = get.VarW(obj)
            value = obj.pVarW;
        end
        %% Define methods
        BackGround = getBackground(obj)
    end
    methods (Access = private)
        obj = UpdateVar(obj)
    end
end

