function [BackGround] = getBackground(obj)
%getBackGround(Video) calculate the BackGround of the Video by calculating
%   the mean over a maximum of a 500 frames. The BackGround is a double
%   vector.

if obj.NumFrames > 500
    N = 500;
else
    N = obj.NumFrames;
end

lengthFrame = prod(obj.SizeFrame);

if obj.Color
    BackGround = zeros(lengthFrame,3);
    for i = 1:3
        for k = 1:N
            BackGround(:,i) = (1-(1/k))*BackGround(:,i) + ...
                (1/k)*obj.frameVector(k,i);
        end
    end
else
    BackGround = zeros(lengthFrame,1);
    for k = 1:N
        BackGround = (1-(1/k))*BackGround + (1/k)*obj.frameVector(k);
    end
end

end

% function [BackGround] = calcBackround(obj,x0,N)
% BackGround = x0;
% for k = 1:N
%     BackGround = (1-(1/k))*BackGround + (1/k)*obj.frameVector(k);
% end
% 
% end