function obj = UpdateVar(obj)
%UpdateVar(obj) update the variance values for a VideoData object. These
%   values will be used for the covariance matrices of the Kalman filter.

if obj.Color
    numLayers = 3;
else
    numLayers = 1;
end

obj.pVarW = zeros(1,numLayers);
obj.pVarP = zeros(1,numLayers);

if obj.CorruptTime > 500 % only use past 500 frames for var calculations
    Tstart = obj.CorruptTime-500;
else
    Tstart = 1;
end
for i = 1:numLayers

    %% Calculate variance W
    
    w = zeros(prod(obj.SizeFrame),obj.CorruptTime-2); % initialize
    
    for k = Tstart+1:obj.CorruptTime-1
        w(:,k) = obj.frameVector(k,i) - obj.frameVector(k-1,i); % w[k] = x[k]-x[k-1]
    end
    
    varW = var(w(:));
    if varW<1e-3
        obj.pVarW(i) = 1e-3;
    else
        obj.pVarW(i) = varW;
    end
    
    %% Calculate variance P
    
    % note : adjust to maybe only last x number of frames
    frames = zeros(prod(obj.SizeFrame),obj.CorruptTime-1);
    for k = Tstart:obj.CorruptTime-1
        frames(:,k) = obj.frameVector(k,i)-obj.ForeGround*obj.BackGround(:,i);    % extract foreground
    end
    
    obj.pVarP(i) = var(frames(:));%mean(var(frames'));
    
end