function [Frames] = getFramesGray(v,varargin)
%getFramesGray(v) OR saveVideoData(v,N) OR saveVideoData(v,N,window)saves the
%   frames of the video object v in a cell. It also converts from RGB to
%   grayscale.

%% Determine number of frames to extract
if nargin==1
    N = v.NumFrames;
elseif nargin == 2
    N = varargin{1};
elseif nargin == 3
    N = varargin{1};
    window = varargin{2};
end

%% Read and save all the files in a struct

Frames = cell(N,1);
for k = 1:N
    Frames{k} = rgb2gray(read(v,k));    
    if nargin == 3
        Frames{k} = Frames{k}(window.offset(1):window.offset(1)+window.Size(1),...
            window.offset(2):window.offset(2)+window.Size(2));
    end
end

end