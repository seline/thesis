function [frame] = reshapeFrame(frameVec,frameSize)
%reshapeFrame(frameVec,frameSize)

frame = reshape(uint8(frameVec),frameSize);