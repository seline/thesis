function frame = extractFrame(video,k,varargin)
%extractFrame(video,k) or extractFrame(video,k,window,offset) 


% read frames at time instance frameSelect
frame = read(video,k);

if size(frame,3) == 3
    frame = rgb2gray(frame);
end
if nargin > 2
    window = varargin{1};
    offset = varargin{2};
    frame = frame((offset(1):offset(1)+window(1)-1),...
        (offset(2):offset(2)+window(2)-1),:,:);
    frame = reshape(frame,window(1)*window(2),1);
else
    frame = reshape(frame,[],1);
end


frame = double(frame);
end