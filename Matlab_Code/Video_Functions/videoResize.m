function [v_resize] = videoResize(v,window,directory,profile)

v_resize = VideoWriter(directory,profile);
v_resize.FrameRate = v.FrameRate;

offset = window.Offset;
SizeFrame = window.Size;

open(v_resize)
for k = 1:v.NumFrames
    frame = read(v,k);
    frame = frame(offset(1):offset(1)+SizeFrame(1)-1,...
        offset(2):offset(2)+SizeFrame(2)-1);
    writeVideo(v_resize,frame);
end
close(v_resize);

end

% 
% v = VideoReader(video);
% 
% frames = read(v,[1 Inf]);
% 
% sz = size(frames);
% 
% if length(sz)==4    %RGB video
%     N = sz(4);
%     frameResize = zeros(W,H,3,N);
%     for k = 1:N
%        frameResize(:,:,:,k) = imresize(frames(:,:,:,k),[W H]); 
%     end
% elseif length(sz)==3 %Grayscale video
%     N = sz(3);
%     frameResize = zeros(W,H,N);
%     for k = 1:N
%        frameResize(:,:,k) = imresize(frames(:,:,k),[W H]); 
%     end
% end
% 
% v_resize = VideoWriter(directory);
% v_resize.FrameRate = v.FrameRate;
% 
% open(v_resize)
% writeVideo(v_resize,frameResize)
% close(v_resize)
% 
