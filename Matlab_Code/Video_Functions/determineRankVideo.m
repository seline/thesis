
v = VideoReader('videos/TownCentreXVID_480p_Gray.avi');
numFrames = 1000;

%%
video = VideoData(v,numFrames);
frameSize = video.SizeFrame;

%%
frame = video.frameVector(1); % change to corruption time
frameFG = frame - video.BackGround;
%%
q = [5 3 2 2 2 2 2 2 2 2 2 3 3 5];
%q = [2 2 2 2 3 3 5 5 3 2 2 2 2 2];
d = length(q);
rankTT = setRank(d,10,50,'l');
frameTT = Vec2TT(frame,q,rankTT,'r');
frameFGTT = Vec2TT(frameFG,q,rankTT,'r');
%%
frameTrunc = TT2Vec(frameTT);
frameTruncFG = TT2Vec(frameFGTT)+video.BackGround;
relError = norm(frameTrunc-frame,'fro')/norm(frame,'fro')
relErrorFG = norm(frameTruncFG-frame,'fro')/norm(frame,'fro')
%% Plotting

figure(1)
imshow(reshape(im2uint8(frameTrunc),frameSize(1),frameSize(2)));

figure(2)
imshow(reshape(im2uint8(frameTruncFG),frameSize(1),frameSize(2)));

figure(3)
imshow(reshape(im2uint8(video.BackGround),frameSize(1),frameSize(2)));

% figure(3)
% stairs(relError)
% hold on
% stairs(relErrorFG)
