function [BackGround] = saveBackground(v)
%getBackGround(Video) calculate the BackGround of the Video by calculating
%   the mean over a maximum of a 500 frames. The BackGround is a double
%   vector.

if strcmp(v.VideoFormat,'Grayscale')
    numLayers = 1;
else
    numLayers = 3;
end

sizeFrame = [v.Height, v.Width];
lengthFrame = prod(sizeFrame);
BackGround = zeros(lengthFrame,numLayers);

for k = 1:6000
    frame = double(read(v,k));
    frameVector = reshape(frame,sizeFrame(1)*sizeFrame(2),[]);
    for ii = 1:numLayers
        BackGround(:,ii) = (1-(1/k))*BackGround(:,ii) + ...
            (1/k)*frameVector(:,ii);
    end
end


end

% function [BackGround] = calcBackround(obj,x0,N)
% BackGround = x0;
% for k = 1:N
%     BackGround = (1-(1/k))*BackGround + (1/k)*obj.frameVector(k);
% end
% 
% end