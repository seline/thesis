function rgb2gray_Video(rgb_video,name)



gray_video = VideoWriter(name,'Grayscale AVI');
gray_video.FrameRate = rgb_video.FrameRate;
open(gray_video);

while rgb_video.hasFrame
    frame = rgb2gray(readFrame(rgb_video));
    writeVideo(gray_video,frame);
end

close(gray_video);

end

% v = VideoWriter('peaks.avi');
% open(v);
% Generate a set of frames, get the frame from the figure, and then write each frame to the file.
% 
% for k = 1:20 
%    surf(sin(2*pi*k/20)*Z,Z)
%    frame = getframe(gcf);
%    writeVideo(v,frame);
% end
% 
% close(v);