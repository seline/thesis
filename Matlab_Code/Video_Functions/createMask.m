function [measIndex,mask] = createMask(sizeFrame,measPercentage,varargin)
%[measIndex,mask] = createMask(sizeFrame,measFrames) generates a sparse vector that has 1's on
%   the pixel indices that are measured and zero when they are not.


lengthFrame = sizeFrame(1)*sizeFrame(2);
% Generate selected indices
measIndex = randperm(lengthFrame,ceil(measPercentage*lengthFrame));
measIndex = sort(measIndex);
% Generate
if nargout == 2
    mask = sparse(lengthFrame,1);
    mask(measIndex) = 1;
end
if nargin == 3
    switch varargin{1}
        case 'm'
            mask = reshape(mask,sizeFrame(1),sizeFrame(2));
            [I,J] = ind2sub(sizeFrame,measIndex);
            measIndex = [I', J'];
        case 'f'
            if nargout ==2
                mask = full(mask);
            end
        otherwise
           warning('This option is not specified for createMask function, now using default');
    end
end

end
