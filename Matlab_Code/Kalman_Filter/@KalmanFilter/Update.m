function obj = Update(obj,k)
%Update(obj,k) performs the Kalman filter update step. 'k' is the
%   current time-step.

if obj.Color
    numLayers = 3;
else
    numLayers = 1;
end


switch obj.Options.Type
    case 'TT'
        x = cell(numLayers,1);
        P = cell(numLayers,1);
        W = cell(numLayers,1);
        if obj.Color
            
            parfor i = 1:numLayers
                
                [x{i},P{i},W{i}] = ...
                    kalmanUpdateTTPixel(obj.x{i},obj.y{k,i},obj.C,obj.P{i},obj.W{i},...
                    obj.Options); % simplify to
            end
        else
            [x{1},P{1},W{1}] = ...
                kalmanUpdateTTPixel(obj.x{1},obj.y{k,1},obj.C,obj.P{1},obj.W{1},...
                obj.Options); % simplify to
        end
        obj.x = x;
        obj.P = P;
        obj.W = W;
    case 'patch'
        
    case 'full'
        for i= 1:numLayers
            [obj.x{i},obj.P{i},obj.W{i}] = ...
                kalmanUpdateMat(obj.x{i},obj.y{k,i},obj.C,obj.P{i},obj.W{i},obj.Options);
        end
    otherwise
        error("Option for KalmanFilter.Options.Type not valid.");
end


end