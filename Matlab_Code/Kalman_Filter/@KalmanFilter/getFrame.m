function X = getFrame(obj,sz)
%getFrame(obj) outputs the current frame as a vector in case of TT the
%   TT is converted to a vector.

if obj.Color
    numLayers = 3;
else
    numLayers = 1;
end
X = zeros(sz,numLayers);
for i = 1:numLayers

    switch obj.Options.Type
        case 'TT'
            X(:,i) = TT2Vec(obj.x{i});
        case 'full'
            X(:,i) = obj.x{i};
        otherwise
            error("Option for KalmanFilter.Options.Type not valid");
    end

end