function [x,P,W_new] = kalmanUpdateTTPixel(x,y,C,P,W_old,Options)
%kalmanUpdateTTPixel(x,y,C,P_old,W,Options) calculates the Kalman update 
%   for the given parameters. In this function the parameters are given as 
%   tensor-trains.
%
%INPUT
%   x : state-vector (as tensor-train).
%   y : measured pixels (of this time-step).
%   C : measurement indices.
%   P : covariance matrix previous time-step.
%   W_old : process covariance matrix.
%   Options : Kalman filter Options struct.
%OUTPUT
%   x : updated state-vector.
%   P : updated covariance matrix.
%   W_new : (updated) process covariance.

%% Set Options
rankX = Options.Rank.x;
rankP = Options.Rank.P;
szX = size(x);
lengthX = prod(szX(:,2));

if Options.W.Update
    % copy data
    W_new.W0 = W_old.W0;
    % calculate W
    W_old = W_old.varW * W_old.W0;
end
%% Prediction step
x_old = x;
P = P + W_old;
P = rounding(P,rankP,'r');

numOutputs = length(C);
K = cell(numOutputs,1);
KSK = cell(numOutputs,1);
v = zeros(numOutputs,1);

for ii = 1:numOutputs
    %% Prediction
    % output residual
    measIndex = C(ii);
    x_ii = extractElementTT(x,measIndex);
    v(ii,1) = y(ii,1) - x_ii;  %scalar
    % Kalman steps
    PC = extractColTTm(P,measIndex); %P*C'
    S = extractElementTT(PC,measIndex);% S scalar 
    K{ii} = PC*(1/S);     % rank K = rank PC 
    KSK{ii} = (K{ii}*K{ii})*(-S); % -K*K' * S (S is scalar)

    %% Update (summation)
    x = x + K{ii}*v(ii,1);
    if any(rank(x)>Options.Rank.maxX)   
        x = rounding(x,rankX,'r');
    end
    P = P + KSK{ii};
    if any(rank(P)>Options.Rank.maxP) 
        P = rounding(P,rankP,'r');
    end
end
x = rounding(x,rankX,'r');
P = rounding(P,rankP,'r');

if Options.W.Update
    % save new W
    warning off;
    W_new.varW = (1/(lengthX-1))*norm(x-x_old)^2;
    warning on;
else 
    W_new = W_old;
end

end
