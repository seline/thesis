function [x_new,P,W_new] = kalmanUpdateMat(x_old,y,C,P,W,Options)
%kalmanUpdateMat(x,y,C,P,W,Options) calculates the Kalman update for the given
%   parameters. In this function the parameters are given as vectors and
%   matrices (NOT tensor-trains).

%% Prediction step
% x = A x + B u = x  and P = A P A' + W = P + W
if Options.W.Update
    W_new.W0 = W.W0;
    W = W.normW * W.W0;
end

P = P + W;

%% Update step
% Measurement residual
v = y - C*x_old;
% Residual covariance matrix
S = C*P*C'; % noise-free measurement
% Kalman gain
K = P*C'/S;

%% Update estimates
x_new = x_old + K*v;
P = P - K*S*K';

%% Update W if specified

if Options.W.Update
    W_new.normW = norm(x_new-x_old,'fro')^2;
else
    W_new = W;
end



