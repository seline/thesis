function [C_TT] = createCTT(frameSize,measIndex,q)
%createCTT(frameSize,measIndex,q) create the TensorTrain C-matrix in the
%   form of a cell structure. Where is element of the cell represents one row
%   of the C-matrix.
%
%INPUT
%   frameSize : the size of the frame [heigth width].
%   measIndex : indices of the measured pixels. From createMask function.
%   q : quantization parameter.
%OUTPUT
%   C_TT : cell with TT rows of C-matrix.


frameLength = prod(frameSize);
N = length(measIndex);
C_TT = cell(N,1);

parfor k = 1:N
    C = zeros(1,frameLength);
    C(:,measIndex(k))=1;
    C_TT{k} = Vec2TT(C,q,1,'r');
end
