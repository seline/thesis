function [x,P,W,y,C,mask] = initializeKalmanPatch(video,Options)

%% Initialize W and P
W = initializeCovTT(video.SizeFrame,Options.Rank.P,video.VarW,Options.q,Options.W);
P = initializeCovTT(video.SizeFrame,Options.Rank.P,video.VarP,Options.q,Options.P);
if Options.W.Update
    normW = norm(W);
    W0 = W*(1/normW);
    W = struct('W0',W0,'normW',normW);
end

%% Calculate y
% generate mask
[measIndex,mask] = createMask(video.SizeFrame,1-Options.MissingPixels);
qTot = [Options.q.Height, Options.q.Width];

patchSize = 128;
qPatch = 2*ones(1,log(patchSize)/log(2));
N = length(measIndex);
if mod(N,patchSize)== 0
    p = N/patchSize;    %number of patches
    lastPatch = patchSize;
else
    p = ceil(N/patchSize);    %number of patches
    lastPatch = mod(N,patchSize);
end

C = createCPatch(video.SizeFrame,measIndex,qTot,qPatch,patchSize);

T_end = video.NumFrames-video.CorruptTime+1;
y = cell(T_end,1);


for k = 1:T_end
    frame = video.frameVector(k+video.CorruptTime-1)-video.BackGround;
    y_k = frame(measIndex);
    y{k} = cell(patchSize,1);
    for i = 1:numPatch-1
        y{k}{i} = y_k(1+(i-1)*patchSize:i*patchSize);
    end
    y{k}{numPatch} = y_k(1+(numPatch-1)*patchSize:(numPatch-1)*patchSize+lastPatch);
end


%% Calculate x
switch Options.x0
    case 'BackGround'
        x = Vec2TT(zeros(prod(video.SizeFrame),1),qTot,1,'r');%video.BackGround;
    case 'LastFrame'
        x = Vec2TT(video.frameVector(video.CorruptTime-1)-video.BackGround,qTot,Options.Rank.x,'r');
end



end

function [C_TT] = createCPatch(frameSize,measIndex,q,qPatch,patchSize)
%createCTT(frameSize,measIndex,q) create the TensorTrain C-matrix in the
%   form of a cell structure. Where is element of the cell represents one row
%   of the C-matrix.
%
%INPUT
%   frameSize : the size of the frame [heigth width].
%   measIndex : indices of the measured pixels. From createMask function.
%   q : quantization parameter.
%   patchSize : size of the patch
%OUTPUT
%   C_TT : cell with TT rows of C-matrix.


frameLength = prod(frameSize);
N = length(measIndex);
if mod(N,patchSize)== 0
    p = N/patchSize;    %number of patches
    lastPatch = patchSize;
else
    p = ceil(N/patchSize);    %number of patches
    lastPatch = mod(N,patchSize);
end
    
C_TT = cell(p,1);

for k = 1:p-1
    C = zeros(patchSize,frameLength);
    for i = 1:patchSize
        C(i,measIndex(i+(k-1)*patchSize))= 1;
    end
    C_TT{k} = Mat2TTm(C,qPatch,q,0,'e');
end

C = zeros(lastPatch,frameLength);
for i = (p-1)*patchSize+1:(p-1)*patchSize+lastPatch
    C(:,measIndex(i)) = 1;
end
% qPatch = 
C_TT{p} = Mat2TTm(C,qPatch,q,0,'e');


end