function factorMat = factorMatrices(sizeFrame,varMat,rangeMat,fun)
%factorMatrices(sizeFrame,varMat,rangeMat,func) calculates the individual
%   matrices that factorize the covariance matrix. The covariance matrix is
%   equal to the kronecker product of all the factor matrices.
%
%INPUT
%   sizeFrame : size of the frame [height width].
%   varMat : variance of covariance matrix.
%   rangeMat : range of influence of pixels.
%   func : interpollation function, value = func(pos,varMat).
%OUTPUT
%   factorMat : cell with all the factor matrices.

factorMat = cell(rangeMat,2);
frameHeight = sizeFrame(1);
frameWidth = sizeFrame(2);

if frameHeight<rangeMat || frameWidth < rangeMat
    error("Pixel range is larger than size of frame.");
end

%% Create factor matrices
for kk = 1:rangeMat
    factorMat{kk,2} = zeros(frameHeight,frameHeight);
    for ii = 1:rangeMat-kk+1
        if ii == 1
            factorMat{kk,2} = factorMat{kk,2} + ...
                fun(ii+kk-1,varMat)*eye(frameHeight);
        else
            factorMat{kk,2} = factorMat{kk,2} + ...
                fun(ii+kk-1,varMat)*diag(ones(frameHeight-ii+1,1),ii-1)+ ...
                fun(ii+kk-1,varMat)*diag(ones(frameHeight-ii+1,1),-(ii-1));
        end
    end
    if kk == 1
        factorMat{kk,1} = eye(frameWidth);
    else
        factorMat{kk,1} = diag(ones(frameWidth-kk+1,1),kk-1)+...
            diag(ones(frameWidth-kk+1,1),-kk+1);
    end
end

