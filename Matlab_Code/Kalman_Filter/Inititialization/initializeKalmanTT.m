function [x,P,W,y,C,mask] = initializeKalmanTT(video,Options)
%initializeKalmanTT(video,Options) initializes the TT Kalman filter
%   parameters.
%
%INPUT
%   video : VideoData object
%   Options : struct with all the Kalman filter options. Use
%       setKalmanOptions to set this struct.
%OUTPUT
%   x : initial state.
%   P : initial covariance matrix.
%   W : process covariance matrix.
%   y : cell with all the measured values.
%   C : measurement matrix.
%   mask : mask of frame.

%% Check if color
if video.Color
    numLayers = 3;
else
    numLayers = 1;
end

%% generate mask and calculate C
[measIndex,mask] = createMask(video.SizeFrame,1-Options.MissingPixels);

qTot = [Options.q.Height, Options.q.Width];
%C = createCTT(video.SizeFrame,measIndex,qTot);

C = measIndex;

%% initialize size
T_end = video.NumFrames-video.CorruptTime+1;
P = cell(1,numLayers);
W = cell(1,numLayers);
y = cell(T_end,numLayers);
x = cell(1,numLayers);

for i = 1:numLayers
    %% Calculate x
    switch Options.x0
        case 'BackGround'
            x{i} = Vec2TT(zeros(prod(video.SizeFrame),1) + ...
                (~video.ForeGround)*video.BackGround(:,i),qTot,Options.Rank.x,'r');%video.BackGround;
        case 'LastFrame'
            x{i} = Vec2TT(video.frameVector(video.CorruptTime-1,i)-video.ForeGround *...
                 video.BackGround(:,i),qTot,Options.Rank.x,'r');
    end
    
    %% Initialize W and P
    
    switch Options.x0
        case 'BackGround'
            x0 = video.frameVector(video.CorruptTime-1,i)-...
                 video.BackGround(:,i);
            varP = var(x0);
        case 'LastFrame'
            varP = 1;
            
    end
    P{i} = initializeCovTT(video.SizeFrame,Options.Rank.P,varP,...
        Options.q,Options.P);
    if Options.W.Update

%         normW = norm(W{i});
%         W0 = W{i}*(1/normW);
%         W{i} = struct('W0',W0,'normW',normW);
        varW = video.VarW(:,i);
        W0 = initializeCovTT(video.SizeFrame,Options.Rank.W,1,...
        Options.q,Options.W);
        W{i} = struct('W0',W0,'varW',varW);
    else
        W{i} = initializeCovTT(video.SizeFrame,Options.Rank.W,video.VarW(:,i),...
        Options.q,Options.W);
    end

    %% Calculate y
    for k = 1:T_end
        frame = video.frameVector(k+video.CorruptTime-1,i)-video.ForeGround ...
            * video.BackGround(:,i);
        y{k,i} = frame(measIndex);% full(nonzeros(frame));
    end

    
end

end