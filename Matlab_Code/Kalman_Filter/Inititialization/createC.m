function C = createC(frameSize,measIndex)
%createC(frameSize,measIndex) creates a C measurement matrix with 1's at
%   the measured pixel values specified by measIndex.
N = length(measIndex);
frameLength = frameSize(1)*frameSize(2);

C = zeros(N,frameLength);%spalloc(N,frameLength,N);

for k = 1:N
   C(k,measIndex(k)) = 1; 
end



end