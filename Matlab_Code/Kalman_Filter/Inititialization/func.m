function [factor] = func(pos,varMat)
%func(pos,varMat) is the interpolation function that decides the value in
%   the covariance matrix. 'pos' stand for the distance to the diagonal of 
%   the matrix and thus the distance between pixels.
%INPUT
%   pos : distance to diagonal, also distance between pixels.
%   varMat : variance covariance matrix (on diagonal).

%% specify function
  factor = varMat/pos;
%   if pos == 1
%       factor = varMat;
%   else
%      factor = varMat * 0.62^(pos);
%   end
end