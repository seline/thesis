function [CovMat,varargout] = initializeCov(sizeFrame,varMat,opt,varargin)
%initializeCov(sizeFrame,varMat,'diag') OR 
%   initializeCov(sizeFrame,varMat,'dist',range,func) initializes the
%   covariance matrix for the Kalman filter.
%
%INPUT
%   sizeFrame : size of the frame [width height].
%   varMat : variance of the covariance matrix (scalar value)
%   opt : 'diag' or 'dist'
%   range (only for 'dist') : range of pixel influence.
%   func : interpollation function, must be a function handle and must have
%       two inputs : position and variance.
%OUTPUT
%   CovMat : covariance matrix.

switch opt
    case 'diag'
        CovMat = kron(eye(sizeFrame(1)),varMat*eye(sizeFrame(2)));
    case 'dist'
        range = varargin{1};
        func = varargin{2};
        factorMat = factorMatrices(sizeFrame,varMat,range,func);
        CovMat = zeros(prod(sizeFrame));
        for k = 1:range
            CovMat = CovMat + kron(factorMat{k,1},factorMat{k,2}); 
        end
    case 'simple_dist'
        range = varargin{1}+1;
        frameHeight = sizeFrame(1);
        frameWidth = sizeFrame(2);
        
        w1 = linspace(varMat,0,range+1);
        w2 = linspace(1,0,range+1);
        
        W1 = varMat*eye(frameHeight);
        W2 = eye(frameWidth);
        
        for ii = 1:range-1
            
            W1 = W1 + diag(w1(ii+1) * ones(frameHeight-ii,1),ii) + ...
                diag(w1(ii+1) * ones(frameHeight-ii,1),-ii);
            
            W2 = W2 + diag(w2(ii+1)*ones(frameWidth-ii,1),ii) + ...
                diag(w2(ii+1)*ones(frameWidth-ii,1),-ii);
        end
        
        try chol(W1) && chol(W2);
            
        catch ME
            warning('Process covariance W not positive definite');
        end
        
        CovMat = kron(W2,W1);
        varargout{1} = W2;
        varargout{2} = W1;
    otherwise
        error('Option specified is not valid');
end

end


