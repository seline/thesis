function [x,P,W,y,C,mask] = initializeKalmanFull(video,Options)
%initializeKalmanFull(video,Options) initializes the full Kalman filter
%   parameters.
%
%INPUT
%   video : VideoData object
%   Options : struct with all the Kalman filter options. Use
%       setKalmanOptions to set this struct.
%OUTPUT
%   x : initial state.
%   P : initialize covariance matrix.
%   W : process covariance matrix.
%   y : cell with all the measured values.
%   C : measurement matrix.

%% Check if color
if video.Color
    numLayers = 3;
else
    numLayers = 1;
end

%%
% generate mask
[measIndex,mask] = createMask(video.SizeFrame,1-Options.MissingPixels,'f');
C = createC(video.SizeFrame,measIndex);

%% initialize size
T_end = video.NumFrames-video.CorruptTime+1;
P = cell(1,numLayers);
W = cell(1,numLayers);
y = cell(T_end,numLayers);
x = cell(1,numLayers);

for i = 1:numLayers
    
    %% Initialize W and P
    switch Options.W.Type
        case 'dist'
            W{i} = initializeCov(video.SizeFrame,video.VarW(:,i),Options.W.Type,Options.W.Range,Options.W.Func);
        case 'simple_dist'
            W{i} = initializeCov(video.SizeFrame,video.VarW(:,i),Options.W.Type,Options.W.Range);
        case 'diag'
            W{i} = initializeCov(video.SizeFrame,video.VarW(:,i),Options.W.Type);
    end
    switch Options.P.Type
        case 'dist'
            P{i} = initializeCov(video.SizeFrame,video.VarP(:,i),Options.P.Type,Options.P.Range,Options.P.Func);
        case 'simple_dist'
            P{i} = initializeCov(video.SizeFrame,video.VarP(:,i),Options.P.Type,Options.P.Range);
        case 'diag'
            P{i} = initializeCov(video.SizeFrame,video.VarP(:,i),Options.P.Type);
    end
    
    if Options.W.Update
        normW = norm(W{i},'fro');
        W0 = W{i}/normW;
        W{i} = struct('W0',W0,'normW',normW);
    end
    
    
    %% Calculate y
    
    for k = 1:T_end
        y{k,i} = C * (video.frameVector(k+video.CorruptTime-1,i) - video.ForeGround ...
            * video.BackGround(:,i));
    end
    
    
    %% Calculate x
    switch Options.x0
        case 'BackGround'
            x{i} = zeros(prod(video.SizeFrame),1) + (~video.ForeGround) * ...
                video.BackGround(:,i);
        case 'LastFrame'
            x{i} = video.frameVector(video.CorruptTime-1,i) - video.ForeGround ...
                * video.BackGround(:,i);
    end
    
end



end