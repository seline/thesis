function [CovMat,varargout] = initializeCovTT(sizeFrame,rankMat,varMat,q,Options)
%initializeCovTT(sizeFrame,varMat,q,Options) initializes the
%   covariance matrix for the Kalman filter.
%
%INPUT
%   sizeFrame : size of the frame [width height].
%   rankMat : TT-rank of matrix
%   varMat : variance of the covariance matrix (scalar value)
%   q : 'diag' or 'dist'
%   Options : struct with parameters 'Type' and (optionally) 'Range'and
%   'Func'.
%OUTPUT
%   CovMat : covariance matrix in TTmatrix format.

qRow = q.Width;
qCol = q.Height;

switch Options.Type
    case 'diag'
        Mat1 = Mat2TTm(eye(sizeFrame(1)),qCol,qCol,0,'e');
        Mat2 = Mat2TTm(varMat*eye(sizeFrame(2)),qRow,qRow,0,'e');
        CovMat = tkron(Mat2,Mat1);
    case 'dist'
        range = Options.Range;
        func = Options.Func;
        factorMat = factorMatrices(sizeFrame,varMat,range,func);
        CovMat = kronCov(factorMat,qRow,qCol);
    case 'simple_dist'
        range = Options.Range+1;
        frameHeight = sizeFrame(1);
        frameWidth = sizeFrame(2);
        
        w1 = linspace(varMat,0,range+1);
        w2 = linspace(1,0,range+1);
        
        W1 = varMat*eye(frameHeight);
        W2 = eye(frameWidth);
        
        for ii = 1:range-1
            
            W1 = W1 + diag(w1(ii+1) * ones(frameHeight-ii,1),ii) + ...
                diag(w1(ii+1) * ones(frameHeight-ii,1),-ii);
            
            W2 = W2 + diag(w2(ii+1)*ones(frameWidth-ii,1),ii) + ...
                diag(w2(ii+1)*ones(frameWidth-ii,1),-ii);
        end
        W1_TT = Mat2TTm(W1,qCol,qCol,rankMat,'r');
        W2_TT = Mat2TTm(W2,qRow,qRow,rankMat,'r');
        CovMat = tkron(W2_TT,W1_TT);
        varargout{1} = W2_TT;
        varargout{2} = W1_TT;
    otherwise
        error('Option specified is not valid');
end
end


function [covMat] = kronCov(factorMat,q1,q2)
%kronCov(factorMat,q1,q2) computes the kronecker product of the factor
%   matrices. End result is a TTm with dimensions quantized by q1 and q2.

numFactors = size(factorMat,1);
W1 = Mat2TTm(factorMat{1,1},q1,q1,0,'e');
W2 = Mat2TTm(factorMat{1,2},q2,q2,0,'e');
covMat = tkron(W1,W2);
for k = 2:numFactors
    W1 = Mat2TTm(factorMat{k,1},q1,q1,0,'e');
    W2 = Mat2TTm(factorMat{k,2},q2,q2,0,'e');
    covMat = covMat + tkron(W1,W2);    
    covMat = rounding(covMat,0,'e');    % edit if too slow or not enough memory
end

end