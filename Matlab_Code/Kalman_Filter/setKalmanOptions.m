function [options] = setKalmanOptions(video,varargin)
%setKalmanOptions(video) OR setKalmanOptions(video,qHeight,qWidth,rankX,rankP)
%   sets the Kalman filter options in a struct. You will be prompted for
%   all the remaining options.
%
%INPUT
%   video : VideoData object.
%   qHeight (opt.) : quantization parameter height of the frame.
%   qWidth (opt.) : quantization parameter width of the frame.
%   rankX (opt.) : rank of TT of frame.
%   rankP (opt.) : rank of TTm's for covariance matrices.
%OUTPUT
%   options : struct with all the necessary Kalman filter options.

if nargin == 1
    prompt = 'TT or full?: ';
    options.Type = input(prompt,'s');
elseif nargin == 5
    options.Type = 'TT';
else
    error('Not enough input arguments');
end

prompt = 'Percentage missing pixels [%]: ';
options.MissingPixels = input(prompt)/100;

prompt = 'Set x0. [BackGround/LastFrame]: ';
options.x0 = input(prompt,'s');

switch options.Type
    case 'TT'
        if nargin == 1
            prompt = 'Quantization parameter for height of frame: ';
            options.q.Height = input(prompt);
            prompt = 'Quantization parameter for width of frame: ';
            options.q.Width = input(prompt);
            
            if (prod(options.q.Height) ~= video.SizeFrame(1)) || (prod(options.q.Width) ~= video.SizeFrame(2))
                error('Invalid quantization parameters, try again.')
            end
        elseif nargin == 5
            options.q.Height = varargin{1};
            options.q.Width = varargin{2};
        end
        
        prompt = 'Type of W? [diag/dist]: ';
        options.W.Type = input(prompt,'s');
        if strcmp(options.W.Type,'dist')
            prompt = 'Range of W: ';
            options.W.Range = input(prompt);
            prompt = 'Interpollation function (use function handle): ';
            options.W.Func = input(prompt);
        elseif strcmp(options.W.Type,'simple_dist')
            prompt = 'Range of W: ';
            options.W.Range = input(prompt);
        end
        prompt = 'Update W? [true/false]: ';
        options.W.Update = input(prompt);
        prompt = 'Type of P? [diag/dist]: ';
        options.P.Type = input(prompt,'s');
        if strcmp(options.P.Type,'dist')
            prompt = 'Range of P: ';
            options.P.Range = input(prompt);
            prompt = 'Interpollation function (use function handle): ';
            options.P.Func = input(prompt);
        elseif strcmp(options.W.Type,'simple_dist')
            prompt = 'Range of P: ';
            options.P.Range = input(prompt);
        end
        if nargin == 1
            prompt = 'Rank of x: ';
            options.Rank.x = input(prompt);
            prompt = 'Rank of P and W: ';
            options.Rank.P = input(prompt);
        elseif nargin == 5
            options.Rank.x = varargin{3};
            options.Rank.P = varargin{4};
        end
        options.Rank.maxP = 5; % change 
        options.Rank.maxX = 50;
    case 'full'
        
        prompt = 'Type of W? [diag/dist]: ';
        options.W.Type = input(prompt,'s');
        if strcmp(options.W.Type,'dist')
            prompt = 'Range of W: ';
            options.W.Range = input(prompt);
            prompt = 'Interpollation function (use function handle): ';
            options.W.Func = input(prompt);
        elseif strcmp(options.W.Type,'simple_dist')
            prompt = 'Range of W: ';
            options.W.Range = input(prompt);
        end
        prompt = 'Update W? [true/false]: ';
        options.W.Update = input(prompt);
        prompt = 'Type of P? [diag/dist]: ';
        options.P.Type = input(prompt,'s');
        if strcmp(options.P.Type,'dist')
            prompt = 'Range of P: ';
            options.P.Range = input(prompt);
            prompt = 'Interpollation function (use function handle): ';
            options.P.Func = input(prompt);
        elseif strcmp(options.W.Type,'simple_dist')
            prompt = 'Range of P: ';
            options.P.Range = input(prompt);
        end
        
end