function [options] = setDefaultOptions(video,missingPixels,qHeight,qWidth,rankX,rankP,rankW, bandWidth)
%setDefaultOptions(video,missingPixels,qHeight,qWidth,rankX,rankP,rankW,bandWidth)
%   sets the Kalman filter options in a struct. You will be prompted for
%   all the remaining options.
%
%INPUT
%   video         : VideoData object
%   missingPixels : percentage missing pixels, as a fraction
%   qHeight       : quantization parameter heigth of frame, as a row-vector
%   qWidth        : quantization parameter width of frame, as row-vector
%   rankX         : maximum rank of x (video frame)
%   rankP         : maximum rank of P (state covariance matrix)
%   rankW         : rank of W (process covariance)
%   bandWidth     : bandwidth of W
%
%OUTPUT
%   options : struct containing all the options needed for the Kalman
%             filter.

            
if (prod(qHeight) ~= video.SizeFrame(1)) || (prod(qWidth) ~= video.SizeFrame(2))
    error('Invalid quantization parameters, try again.')
end

options.Type = 'TT';
options.MissingPixels = missingPixels;
options.x0 = 'LastFrame';
options.q.Height = qHeight;
options.q.Width = qWidth;
options.W.Type = 'simple_dist';
options.W.Range = bandWidth;
options.W.Update = true;
options.P.Type = 'simple_dist';
options.P.Range = bandWidth;
options.Rank.x = rankX;
options.Rank.P = rankP;
options.Rank.W = rankW;
options.Rank.maxP = 5;
options.Rank.maxX = 50;

end