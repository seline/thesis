function [X] = PLMS(M,J,X0,lambda, mu)
%PLMS(M,J,X0,lambda,mu) implements the PLMS adaptive matrix completion
%   algorithm. 
%
%INPUT
%   M : cell(T,1), matrix sequence of corrupted frames.
%   J : cell(T,1), matrix sequence with location of uncorrupted pixels.
%   X0 : initial frame
%   lambda : 
%   mu :
% 
%OUPUT
%   X : cell(T,1), matrix sequence of completed frames.

T = length(M);
X = cell(1,T);
X{1} = X0;

for t = 1:T-1
    X{t+1} = SVToperator((X{t}+mu*J{t}.*(M{t}-X{t})),lambda*mu);
end