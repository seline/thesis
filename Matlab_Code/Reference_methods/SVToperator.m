function [D] = SVToperator(A,lambda)
%SVToperator(A,lambda) calculates the SVT operator of a matrix A.
%   D_{lambda}(A) = U*D_{lambda}(S)*V', D_{lambda}(S) =
%   diag([s_1-lambda],...);
%INPUT
%   A : matrix
%   lambda : thresholding parameter
%OUTPUT
%   D(A) : SVT operator of A

[U,S,V] = svd(A,'econ');
DS = shrinkage(S,lambda);
D = U*DS*V';
