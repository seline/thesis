function [Sx] = shrinkage(x,tau)
%shrinkage(x,tau) computes the shrinkage operator.
%
%INPUT
%   x : variable to `shrink'
%   tau : shrinkage parameter
%OUTPUT
%   Sx : shrinkage operator


Sx = sign(x).*max(abs(x)-tau,0);